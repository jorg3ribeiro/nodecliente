const app = require('express')()
const consign = require('consign')
const path = require('path');
const express = require('express')
require('./config/database')()


consign()
    .then('./config/middlewares.js')
    .then('./services/validationServices.js')
    .then('./services')
    .include('./config/passport.js')
    .then('./controllers') 
    .then('./routes')
    .into(app)


app.set('views', path.join(__dirname, '/views/pages/'));
app.set('view engine', 'pug');


app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '/views/public')));

app.get('/', function(req, res){
    res.render('login',  {title: 'Login'} )
})
app.get('/register', function(req, res){
    res.render('register')
})

app.get('/index', function(req, res){
    res.render('index')
})

app.get('/cliente', function(req, res){
    res.render('clientes')
})

app.listen(3000, () => {
    console.log('Backend executando...')
})

module.exports = app