const bodyParser = require('body-parser')
let logger = require('morgan');
const cors = require('cors')
const compression = require('compression')

module.exports = app => {
    app.use(bodyParser.json())
    app.use(cors())
    app.use(compression())    
    app.use(logger('dev'));
}