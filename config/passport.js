const { authSecret } = require('../.env')
const passport = require('passport')
const passportJwt = require('passport-jwt')
const { Strategy, ExtractJwt } = passportJwt
const mongoose = require("mongoose");
require('../models/userModel')
const User = mongoose.model("users");

module.exports = app => {
    
    const params = {
        secretOrKey: authSecret,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    }
    
    const strategy = new Strategy(params, (payload, done) => {
        let users;

       const resultUsers = User.findOne({ "_id": payload._i }).exec()            
        if(resultUsers == null){
            users = {
               created: false
            }
        }else{
            users = {
                userEmail: JSON.parse( JSON.stringify(resultUsers)),
                created: true
            }
        }

        if(users.created){
           return done(null, users.userEmail ? { ...payload } : false)
        }else{
            return done(err, false)
        }
    })

    passport.use(strategy)

    return {
        authenticate: () => passport.authenticate('jwt', { session: false })
    }
    
}