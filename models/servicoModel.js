const mongoose = require('mongoose');

const servicoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }, 
    createdAt:{
        type: Date,
        default: Date.now
    },
})

const Servico = mongoose.model('servicos', servicoSchema)
module.exports = Servico;