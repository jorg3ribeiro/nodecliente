const mongoose = require('mongoose');

var clienteSchema = new mongoose.Schema({
    name:{
        type: String
    },
    telefone: {
        type: String
    },
    endereco: {
        type: String
    },
    servico: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'servicos'
    },
    createdAt:{
        type: Date,
        default: Date.now
    },
});

const Cliente = mongoose.model('clientes', clienteSchema);
module.exports = Cliente;