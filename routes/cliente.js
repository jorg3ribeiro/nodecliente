const bodyParser = require("body-parser");
module.exports = app => {
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.route('/clientes')
        .post(app.controllers.clienteController.save)
        .get(app.controllers.servicoController.get)
    
        
}