module.exports = app => {
    app.route('/users')
        .all(app.config.passport.authenticate())
        .post(app.controllers.userController.save)
        .get(app.controllers.userController.get)
}