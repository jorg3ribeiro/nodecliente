const bodyParser = require("body-parser");
module.exports = app => {
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.route('/servicos')
        .post(app.controllers.servicoController.save)
}