const bodyParser = require("body-parser");
module.exports = app => {
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.post('/signup', app.controllers.userController.save)
    app.post('/signin', app.controllers.authController.signin)
    app.post('/validadeToken', app.controllers.authController.validateToken)
}