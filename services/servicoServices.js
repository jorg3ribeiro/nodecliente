const mongoose = require("mongoose");
require('../models/servicoModel')
const Servico = mongoose.model("servicos");

module.exports = {

    async saveServico(servico){
        let servicos;
       
        const servicoSave = Servico.create(servico)
        if(servicoSave == null){
            servicos = false
        }else{
            servicos = true
        }
        return servicos
    },

    async findOneServico(servico){
        let servicos;
        const servicoFindOne = await Servico.findOne({ "name": servico }).exec()
             
         if(servicoFindOne == null){
            servicos = false
         }else{
            servicos = true
         }
        
         return servicos
     },

     async findServicos(){
        const ServicoGet = await Servico.find({})
        return ServicoGet
    }
}