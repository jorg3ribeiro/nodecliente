const mongoose = require("mongoose");
require('../models/clienteModel')
const Cliente = mongoose.model("clientes");

module.exports = {

    async saveCliente(cliente){
        let clientes;
        try{
            const clienteSave = Cliente.create(cliente);
            clientes = {
                cliente: clienteSave,
                created: true
            };
        }catch (err){
            clientes = {
                error: "Problema ao cadastrar servico",
                created: false
            }
        }

        return clientes

    },

    async findClientes(){
        const clienteGet = await Cliente.find({})
        return clienteGet
    }

}