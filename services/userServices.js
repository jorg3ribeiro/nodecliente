const mongoose = require("mongoose");
require('../models/userModel')
const User = mongoose.model("users");

module.exports = {
    async saveUsers(user){
        let users;
        try{
            const usersSave = User.create(user);
            users = {
                user: usersSave,
                created: true
            };
        }catch (err){
            users = {
                error: "Problema ao cadastrar usuario",
                created: false
            }
        }

        return users

    },

    async findOneUsers(userEmail){
       let users;
       const resultUsers = await User.findOne({ "email": userEmail }).exec()            
        if(resultUsers == null){
            users = {
               created: false
            }
        }else{
            users = {
                userEmail: JSON.parse( JSON.stringify(resultUsers)),
                created: true
            }
        }
       
        return users
    },

    async findUsers(){
        const userGet = await User.find({})
        return userGet
    },

    async findByIdUser(id){
        let users;
        const resultUsers = await User.findOne({ "_id": id }).exec()            
         if(resultUsers == null){
             users = {
                created: false
             }
         }else{
             users = {
                 userEmail: JSON.parse( JSON.stringify(resultUsers)),
                 created: true
             }
         }
        
         return users
    }
}