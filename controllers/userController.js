const bcrypt = require('bcrypt-nodejs')

module.exports = app => {

    const { existsOrError, notExistsOrError, equalsOrError } = app.services.validationServices

    const userServices = app.services.userServices

    const encryptPassword = password => {
        const salt = bcrypt.genSaltSync(10)
        return bcrypt.hashSync(password, salt)
    }
    const save = async(req, res) => {
        const user = { ...req.body }

        try{
            existsOrError(user.name, 'Nome não informado')
            existsOrError(user.email, 'E-mail não informado')
            existsOrError(user.password, 'Senha não informada')
            existsOrError(user.confirmPassword, 'Confirmação de senha inválida')
            equalsOrError(user.password, user.confirmPassword,
                'Senhas não conferem')

           const userFromDB = await userServices.findOneUsers(user.email)
           if(userFromDB.created){
               notExistsOrError(userFromDB, "Usuário já cadastrado")
           }
        }catch(msg){
            return res.status(400).render('register', {msg: msg, msgNome: user.name, msgEmail: user.email})
        }
        
        user.password = encryptPassword(user.password)
        delete user.confirmPassword

        const userSave = await userServices.saveUsers(user)

        if(userSave.created){
            return res.status(204).redirect('index')
        }else{
            return res.status(500).render('register', { msg: userSave.error })
        }
    }

    const get = async(req, res) => {
        const userGet = await userServices.findUsers()

       return res.json(userGet.map(u => {
            return {"_id": u._id, "name": u.name, "email": u.email, "admin": u.admin}
        }))
    }

    return { save, get }
}