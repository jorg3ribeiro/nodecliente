module.exports = app => {
    const { existsOrError, notExistsOrError } = app.services.validationServices

    const servicoService = app.services.servicoServices

    const save = async(req, res) => {
        const servico = { ...req.body }

        try{
            existsOrError(servico.name, "Nome não informado")

            const servicoFromDB = await servicoService.findOneServico(servico.name)
            if(servicoFromDB){
                notExistsOrError(servicoFromDB, "Servico já cadastrado")
            }
        }catch(msg){
            return res.status(400).render('servicos',{ msg: msg })
        }

        const servicoSave = await servicoService.saveServico(servico)

        const servicoGet = await servicoService.findServicos()
        let servicos = servicoGet.map(s => {
            return s._doc
        })     

        if(servicoSave){
            return res.status(204).render('servicos', {msg: "Servico Criado"})
        }else{
            return res.status(500).render('servicos',{ msg: servicoSave.erro })
        }
    }

    const get = async(req, res) => {
        const servicoGet = await servicoService.findServicos()
        let servicos = servicoGet.map(s => {
            return s._doc
        })      
        return res.render('clientes', { servicos: servicos }  )

    }

    return { save, get }
}