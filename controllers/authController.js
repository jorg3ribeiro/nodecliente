const { authSecret } = require('../.env')
const jwt = require('jwt-simple')
const bcrypt = require('bcrypt-nodejs')
const { token } = require('morgan')

module.exports = app => {
    const userServices = app.services.userServices

    const signin = async(req, res) => {
        if(!req.body.email || !req.body.password){
            return res.status(400).render('login',{ msg:'Informe Usuario e Senha!' })
        }

        const user = await  userServices.findOneUsers(req.body.email)
        if(!user.created) return res.status(400).render('login',{ msg: 'Usuario não encontrado!' })

        const users = { ...user.userEmail}

        const isMatch = bcrypt.compareSync(req.body.password, users.password)
        if(!isMatch) return res.status(401).render('login', { msg: 'Senha inválido!' })

        const now = Math.floor(Date.now() / 1000)

        const payload = {
            id: users._id,
            name: users.name,
            email: users.email,
            admin: users.admin,
            iat: now,
            exp: now + (60 * 60 * 24 * 3)
        }

        res.render('index',{
            ...payload,
            token: jwt.encode(payload, authSecret)
        })
    }

    const validateToken = async(req, res) => {
        const userData = req.body || null
        try{
            if(userData){
                const token = jwt.decode(userData.token)
                if(new Date(token.exp * 1000) > new Date()) {
                    return res.render('index')
                }
            }
        }catch(e){
            //Problema com o token
        }

       res.render('login', {msg: 'Seu token expirou', title: 'Login'})
    }

    return { signin, validateToken}
}