module.exports = app => {
    const { existsOrError } = app.services.validationServices

    const clienteService = app.services.clienteServices
    const servicoService = app.services.servicoServices

    const save = async(req, res) => {
        const cliente = { ...req.body }

        try{
            existsOrError(cliente.name, "Nome não informado")
            existsOrError(cliente.telefone, "Telefone não informado")
            existsOrError(cliente.endereco, "Endereço não informado")
            existsOrError(cliente.servico, "Serviço não escolhido")

        }catch(msg){
            return res.status(400).redirect('clientes', {msg: msg})
        }

        const clienteSave = await clienteService.saveCliente(cliente)

        if(clienteSave.created){
            return res.status(204).redirect('index')
        }else{
            return res.status(500).send('clientes', { msg: clienteSave.error})
        }
    }

    const get = async(req, res) => {
        const clienteGet = await clienteService.findClientes() 

       return res.json(clienteGet.map(c => {
            return {"_id": c._id, "name": c.name, "telefone": c.telefone, "endereco": c.endereco, "serviço": c.parentId}
        }))
    }

   

    return { save, get }
}